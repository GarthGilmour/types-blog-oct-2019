package com.instil.typing.async

import arrow.core.*;
import java.io.File

import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpClient.Redirect.ALWAYS
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.concurrent.CompletableFuture

typealias InvalidTextOrUri = Validated<String, URI>
typealias ExceptionOrInputs = Either<Exception, Set<InvalidTextOrUri>>
typealias InvalidTextOrFuture = Either<String, CompletableFuture<String>>

fun main() {
    fun printError(ex: Exception) = println(errorToString(ex))

    fun processInputs(input: Set<InvalidTextOrUri>): List<CompletableFuture<Unit>> {
        fun wrapInFuture(str: String) = CompletableFuture.completedFuture(str)

        val futures = pingSites(input).map { it.fold(::wrapInFuture, { it }) }
        return futures.map { it.thenApply(::println) }
    }

    fun combineAndJoin(input: Set<InvalidTextOrUri>) = CompletableFuture
        .allOf(*processInputs(input).toTypedArray())
        .join()

    val errorOrNames = readPageNames(readFileName())
    errorOrNames.fold(::printError, ::combineAndJoin)
    println("All done...")
}

fun errorToString(ex: Throwable) = "Error: '${ex.message}'"

fun readFileName(defaultPath: String = "input/sites.txt"): String {
    println("Enter the filename ('$defaultPath')")
    val fileName = readLine() ?: ""
    return if (fileName == "") defaultPath else fileName
}

fun readPageNames(name: String): ExceptionOrInputs = try {
    Either.right(readLinesFromFile(name))
} catch (ex: Exception) {
    Either.left(ex)
}

fun readLinesFromFile(name: String): Set<InvalidTextOrUri> {
    val siteRegex = "http://.+".toRegex()
    return File(name).useLines { lines ->
        lines.map { line ->
            if (line.matches(siteRegex)) {
                Valid(URI(line))
            } else {
                Invalid("$line does not match regex")
            }
        }.toSet()
    }
}

fun pingSites(input: Set<InvalidTextOrUri>): List<InvalidTextOrFuture> {
    val client = HttpClient.newBuilder().followRedirects(ALWAYS).build()
    val handler = HttpResponse.BodyHandlers.ofString()

    fun buildRequest(uri: URI) = HttpRequest.newBuilder().uri(uri).build()
    fun pingSite(uri: URI) = client
        .sendAsync(buildRequest(uri), handler)
        .handle { result, error ->
            result?.statusCode()?.toString() ?: errorToString(error)
        }

    return input.map { invalidOrUri ->
        invalidOrUri.fold({ Left(it) }, { Right(pingSite(it)) })
    }
}



